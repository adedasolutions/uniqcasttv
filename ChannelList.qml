import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import "./js/ApiService.js" as ApiService

Page {
    id: channellist

    ScrollView {
        id: scrollView
        anchors.fill: parent
        anchors.margins: 9

        ListView {
            id: logoview
            model: ListModel {
                id: outputModel
            }
            delegate: ColumnLayout {
                width: ListView.view.width
/*
                Label {
                    id: channelid
                    text: channelsid
                    visible: false
                    TapHandler {
                        onTapped:  ApiService.openChannel(channelid.text);
                    }
                }
*/


                Label {
                    Layout.fillWidth: true
                    color: 'green'
                    anchors.margins: 20
                    font.family: "Helvetica"
                    font.pointSize: parent.width/20
                    anchors.left: logo.right
                    anchors.leftMargin: 250
                    anchors.bottom: logo.bottom
                    anchors.bottomMargin: 40
                    text: channelnames
                    TapHandler {
                        onTapped:  ApiService.openChannel(ApiService.channelId);
                    }
                }
                Rectangle {
                    height: 1
                    Layout.fillWidth: true
                    color: '#333'
                    opacity: 0.2
                    TapHandler {
                        onTapped:  ApiService.openChannel(ApiService.channelId);
                    }
                }
                Image {
                    id: logo
                    source: logosurce
                    asynchronous: true
                    fillMode: Image.PreserveAspectFit
                    anchors.verticalCenter: parent.bottom
                    anchors.left: parent.left
                    anchors.leftMargin: 50
                    TapHandler {
                        onTapped:  ApiService.openChannel(ApiService.channelId);
                    }
                }
            }
        }
    }
    Component.onCompleted: ApiService.getChannels()
}
