import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import "./js/ApiService.js" as ApiService

Page {
    id: login
    Layout.fillWidth: true
    Layout.fillHeight: true
    title: qsTr("Login")    

        Label {
            id: headerLabel
            y: 70
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("Welcome to UniqCastTV")
            font.pixelSize: 32
            font.italic: true
            color: "red"
        }

        Label {
            y: 115
            anchors.right: headerLabel.right
            anchors.rightMargin: 5
            text: qsTr("Please login first to get channels")
            font.pixelSize: 16
            font.italic: true
            color: "red"
        }


        Label {
            id: labelUsername
            y: 250
            anchors.left: username.left
            //anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("Username")
            font.pixelSize: 22
            font.italic: true
            color: "steelblue"
        }

        TextField {
            id: username
            y: 280
            anchors.horizontalCenter: parent.horizontalCenter
            placeholderText: qsTr("Enter username")
        }

        Label {
            id: labelPassword
            y: 350
            anchors.left: password.left
            //anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("Password")
            font.pixelSize: 22
            font.italic: true
            color: "steelblue"
        }

        TextField {
            id: password
            y: 380
            anchors.horizontalCenter: parent.horizontalCenter
            placeholderText: qsTr("Enter password")
            echoMode: TextInput.Password
        }

        Button {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 290
            width: 200
            text: "Log in"
            onClicked: ApiService.login()
        }

}
