import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtMultimedia 5.12
import "./js/ApiService.js" as ApiService


Video {
    id: video
    Layout.fillWidth: true
    Layout.fillHeight: true
    source: "http://devimages.apple.com/iphone/samples/bipbop/gear4/prog_index.m3u8"

    MouseArea {
        anchors.fill: parent
        onClicked: {
            video.play()
        }
    }

    focus: true
    Keys.onSpacePressed: video.playbackState == MediaPlayer.PlayingState ? video.pause() : video.play()
    Keys.onLeftPressed: video.seek(video.position - 5000)
    Keys.onRightPressed: video.seek(video.position + 5000)

    Button {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20
        width: 200
        text: "Back to Channel list"
        onClicked: ApiService.backToChannelList()
    }

     Component.onCompleted: ApiService.startupFunction();

}


