import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import "./js/ApiService.js" as ApiService

Page {
    id: loginFailed
    Layout.fillWidth: true
    Layout.fillHeight: true
    title: qsTr("Login")

        Label {
            id: headerLabel
            y: 300
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("Invalid cridetntals!")
            font.pixelSize: 32
            font.italic: true
            color: "red"
        }

        Button {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 20
            width: 200
            text: "Back"
            onClicked: ApiService.backToLogin()
        }

}
