var jwt;
var videoSource;
var channelId = 563;
var channelName = "DW";

function getChannels() {

    var xmlhttp = new XMLHttpRequest();
    var url = "http://176.31.182.158:3001/channels";

    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            populateListView(xmlhttp.responseText);
        }
    }
    xmlhttp.open("GET", url, true);
    xmlhttp.send();

    headerLabel.text = "Our channels";
}

function populateListView(json) {
    var obj = JSON.parse(json);

    for(var i=0;i<obj.length;i++)
    {
        //logoview.model.append({channelsid: obj[i].id})
        logoview.model.append({logosurce: "https://devel.uniqcast.com/samples/logos/"+obj[i].id +".png"})
        logoview.model.append({channelnames: obj[i].name})

    }
}

function openChannel(id) {

    var xmlhttp = new XMLHttpRequest();
    var url = "http://176.31.182.158:3001/channels/" + id;

    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            var obj = JSON.parse(xmlhttp.responseText);

            videoSource = obj.url;
            channelName = obj.name;

            console.log(channelName);

            stackView.push("../Channel.qml",  {"source": videoSource});
        }
    }
    xmlhttp.open("GET", url, true);
    xmlhttp.send();

}

function login() {

    var objectToSend = {};
        objectToSend.identifier = username.text;
        objectToSend.password = password.text;

    var xmlhttp = new XMLHttpRequest();
    var url = "http://176.31.182.158:3001/auth/local";

    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
           jwt = xmlhttp.responseText;
           stackView.push("../ChannelList.qml");
        }
        if (xmlhttp.readyState === 4 && xmlhttp.status !== 200) {
           jwt = xmlhttp.responseText;
           stackView.push("../LoginFailed.qml");
        }
    }
    xmlhttp.open("POST", url, true);
    xmlhttp.setRequestHeader("Content-type", "application/json");
    xmlhttp.setRequestHeader("Accept", "application/json");
    xmlhttp.send(JSON.stringify(objectToSend));

}

function backToLogin(){
    stackView.push("../Login.qml");
    headerLabel.text = "";
}

function backToChannelList(){
    stackView.pop();
    headerLabel.text = "Our channels";
}

function startupFunction() {
    headerLabel.text = "Channel: " + channelName;
}

function wait(ms){
   var start = new Date().getTime();
   var end = start;
   while(end < start + ms) {
     end = new Date().getTime();
  }
}


