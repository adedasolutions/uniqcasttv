import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import "./js/ApiService.js" as ApiService

ApplicationWindow {
    id: window
    visible: true
    width: 640
    height: 850
    title: qsTr("UniqCastTV")

    header: ToolBar {
        contentHeight: toolButton.implicitHeight
        background: Rectangle {
            color: "#002f3f"
        }

        ToolButton {
            id: toolButton
            text: stackView.depth > 1 ? "\u25C0" : "\u2630"
            font.pixelSize: Qt.application.font.pixelSize * 1.6
            onClicked: {
                if (stackView.depth > 1) {
                    stackView.push("Login.qml")
                    headerLabel.text = "";
                } else {
                    drawer.open()
                }
            }
        }
        Label {
            id: headerLabel
            anchors.centerIn: parent
            font.pixelSize: 20
            elide: Label.ElideRight
            color: "white"
        }

    }

    Drawer {
        id: drawer
        width: Math.min(window.width, window.height) / 3 * 2
        height: window.height

        Column {
            anchors.fill: parent

            ItemDelegate {
                id: loginItem
                text: qsTr("Login")
                width: parent.width
                onClicked: {
                    stackView.push("Login.qml")
                    drawer.close()
                }
            }
            ItemDelegate {
                id: loginFailedItem
                visible: false
                text: qsTr("Login Failed")
                width: parent.width
                onClicked: {
                    stackView.push("LoginFailed.qml")
                    drawer.close()
                }
            }
            ItemDelegate {
                id: channelListItem
                visible: false
                text: qsTr("Channel List")
                width: parent.width
                onClicked: {
                    stackView.push("ChannelList.qml")
                    drawer.close()
                }
            }
            ItemDelegate {
                id: channelItem
                visible: false
                text: qsTr("Channel")
                width: parent.width
                onClicked: {
                    stackView.push("Channel.qml")
                    drawer.close()
                }
            }
        }
    }

    footer: ToolBar {
        contentHeight: toolButton.implicitHeight
        background: Rectangle {
            color: "#002f3f"
        }
    }

    StackView {
        id: stackView
        initialItem: "Login.qml"
        anchors.fill: parent
    }
}
